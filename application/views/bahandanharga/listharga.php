<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data Harga</h3>                                    
            </div><!-- /.box-header -->
            <a data-toggle="modal" href="#myModal" class="btn btn-primary" style="margin-left:10px;"> New Data </a>
            <div class="box-body table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Bahan</th>
                            <th>Ukuran</th>
                            <th>Harga Satuan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach ($tampil as $row) { ?>
                        
                        <tr>
                            <td><?php echo $no ?></td>
                            <td><?php echo $row->bahan; ?></td>
                            <td><?php echo $row->ukuran ;?></td>
                            <td><?php echo $row->harga_satuan;?></td>

                            <td><?php 
                                    echo    anchor(base_url().'bahandanharga/listharga/get_data/'.$row->id, '<i class="glyphicon glyphicon-pencil"></i>',array('title'=>'Edit')). ' | ' .
                                            anchor(base_url().'bahandanharga/listharga/delete_data/'.$row->id, '<i class="glyphicon glyphicon-trash"></i>', array('title'=>'Delete', 'onClick'
                                            => "return confirm('Apakah anda yakin !')"));?>
                            </td>
                        </tr>
                        <?php $no++; } ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Harga</h4>
            </div>
            <?php
                echo form_open(base_url('bahandanharga/listharga/save_data'), $data = array('class' => 'form-horizontal row-border', 'data-validate'=>'parsley', 'id'=>'validate-form' ));
            ?>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Bahan</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="bahan" id="bahan" required>
                            <option value="">-- Pilih Bahan --</option>
                            <?php foreach ($bahan as $row) { ?>
                                    <option value="<?php echo $row->kode_bahan;?>"> <?php echo $row->bahan ?> </option>
                            <?php } ?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Ukuran</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="ukuran" id="ukuran" required>
                            <option value="">-- Pilih Ukuran --</option>
                            <?php foreach ($ukuran as $row) { ?>
                                    <option value="<?php echo $row->kode_ukuran;?>"> <?php echo $row->ukuran ?> </option>
                            <?php } ?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Harga</label>
                        <div class="col-sm-6">
                            <input type="text" name="harga" placeholder="Harga" required="required" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" >Save changes</button>
                </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->