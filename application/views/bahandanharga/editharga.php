<div id="page-heading">
    <ol class="breadcrumb">
        <li><a href="index.php">Dashboad</a></li>
        <li>Form</li>
    </ol>

    <h1>Edit Data</h1>
</div>

<div class="container">

    <div class="panel panel-midnightblue">
        <div class="panel-heading">
            <h4></h4>
        </div>
        <div class="panel-body collapse in">
            <?php
                echo form_open(base_url('bahandanharga/listharga/update_data'), $data = array('class' => 'form-horizontal row-border', 'data-validate'=>'parsley', 'id'=>'validate-form' ));
            ?>
            <?php foreach ($tampil as $isi) { ?>
            
                <div class="form-group">
                    <label class="col-sm-3 control-label">Bahan</label>
                    <div class="col-sm-6">
                        <input type="hidden" name="id" value="<?php echo $isi->id; ?>" placeholder="Kode Barang" required="required" class="form-control" />
                        <input type="text" name="bahan" value="<?php echo $isi->kode_bahan; ?>" required="required" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Ukuran</label>
                    <div class="col-sm-6">
                        <input type="text" name="ukuran" value="<?php echo $isi->kode_ukuran; ?>"  required="required" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Harga</label>
                    <div class="col-sm-6">
                        <input type="text" name="harga_satuan" value="<?php echo $isi->harga_satuan; ?>"  required="required" class="form-control" />
                    </div>
                </div>
                <?php }?>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="btn-toolbar">
                                <button class="btn-primary btn">Submit</button>
                                <button class="btn-default btn">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>

            <?php
                echo form_close();
            ?>
        </div>
    </div>
</div>
