<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data Ukuran</h3>                                    
            </div><!-- /.box-header -->
            <a data-toggle="modal" href="#myModal" class="btn btn-primary" style="margin-left:10px;"> New Data </a>
            <div class="box-body table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Ukuran</th>
                            <th>Nama Ukuran</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach ($tampil as $row) { ?>
                        
                        <tr>
                            <td><?php echo $no ?></td>
                            <td><?php echo $row->kode_ukuran; ?></td>
                            <td><?php echo $row->ukuran ;?></td>

                            <td><?php 
                                    echo    anchor(base_url().'bahandanharga/ukuran/get_data/'.$row->id, '<i class="glyphicon glyphicon-pencil"></i>',array('title'=>'Edit')) . ' | ' .
                                            anchor(base_url().'bahandanharga/ukuran/delete_data/'.$row->id, '<i class="glyphicon glyphicon-trash"></i>', array('title'=>'Delete', 'onClick'
                                            => "return confirm('Apakah anda yakin !')"));?>
                            </td>
                        </tr>
                        <?php $no++; } ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Data Ukuran</h4>
            </div>
            <?php
                echo form_open(base_url('bahandanharga/ukuran/save_data'), $data = array('class' => 'form-horizontal row-border', 'data-validate'=>'parsley', 'id'=>'validate-form' ));
            ?>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Kode Ukuran</label>
                        <div class="col-sm-6">
                            <textarea name="kode_ukuran" placeholder="Kode Ukuran" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Ukuran</label>
                        <div class="col-sm-6">
                            <input type="text" name="nama_ukuran" placeholder="Ukuran" required="required" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" >Save changes</button>
                </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->