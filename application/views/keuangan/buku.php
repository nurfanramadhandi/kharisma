<script type="text/javascript">
    $(document).ready(function() {
        $( "#tanggal" ).datepicker({ dateFormat: "yy-mm-dd" });
    });
</script>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data Transaksi</h3>                                    
            </div><!-- /.box-header -->
            <a data-toggle="modal" href="#myModal" class="btn btn-primary" style="margin-left:10px;"> New Data </a>
            <div class="box-body table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Type</th>
                            <th>Debet</th>
                            <th>Credit</th>
                            <th>Balance</th>
                            <th>Keterangan</th>
                            <th>Tanggal</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach ($tampil as $row) { ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php $row->type;
                                if ($row->type == 1) {
                                    echo "Masuk";
                                }else{
                                    echo "Keluar";
                                }
                             ?></td>
                            <td><?php echo $row->debet ;?></td>
                            <td><?php echo $row->credit ;?></td>
                            <td><?php echo $row->balance ;?></td>
                            <td><?php echo $row->keterangan ;?></td>
                            <td><?php echo $row->tanggal ;?></td>
                        </tr>
                        <?php $no++; } ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

