<script src="<?php echo base_url();?>assets/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        window.print();
    });
</script>
<section class="content invoice" style="width:100%;">                    
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header" style="font-size: 12px;">
                KHARISMA AMALIA 
            </h2>                            
        </div><!-- /.col -->
		<div class="col-xs-12" style=" text-align: left; font-size: 10px;">
			Jl.  Letnan Marsaid No. 33 Lantai 2, Bekasi. 
			</div>
		<div class="col-xs-12" style=" text-align: left; font-size: 10px;">
			Telp. (021) 3317 4538
			</div>
		<hr>
		<div class="col-xs-12" style=" text-align: right; ">
			<small class="pull-right" style=" font-size: 9px;"><?php echo date('d-m-Y h:i:s')?></small>
		</div>
    </div>
    <div class="row">
        <div class="col-xs-12" style="font-size: 10px;">
			<p style="font-size: 12px;">
                No. Struk : <?php echo $isi[0]->voucher; ?>
            </p>
            <p style="font-size: 12px;">
                Costumer : <?php echo $isi[0]->customer; ?>
            </p>                            
        </div><!-- /.col -->
    </div>
		<hr>
    <div class="row">
        <div class="col-xs-12 table-responsive" >
            <table class="table table-striped" style=" width: 100%; ">
            	<thead>
                    <tr>
                        <th style="font-size: 12px;">Pembelian</th>
                        <th style="font-size: 12px;text-align: right;">Harga</th>
                    </tr>                                    
                </thead>
                <tbody>
                    <?php $total=0; foreach ($isi as $row){ ?>
                    <tr>
                        <td style="font-size: 10px;"><?php echo $row->jenis_kasir.'-'.$row->ukuran.'-'.$row->bahan; ?></td>
                        <td style="font-size: 10px;text-align: right;"><?php echo number_format($row->harga_satuan * $row->qty); ?></td>
                    </tr>
                    <tr>
                        <td style="font-size: 10px;"><?php if($row->finishing_qty!=0){echo $row->nama_finishing.' ('.$row->harga_finishing.') X '.$row->finishing_qty ;} ?></td>
                        <td style="font-size: 10px;text-align: right;"><?php if($row->finishing_qty!=0){echo number_format($row->harga_finishing * $row->finishing_qty);} ?></td>
                    </tr>
                    <?php $total = $row->harga_total + $total; } ?>
                </tbody>
				<tr>
					<td></td>
					<td style="text-align: right;font-size: 10px;">------------</td>
				</tr>
				<tr>
					<td style="font-size: 12px;">
                Total : </td>
					<td style="text-align: right;font-size: 12px;"><?php echo 'Rp. '.number_format($total)?></td>
				</tr>
            </table>
			<div class="col-xs-12" style=" text-align: center; font-size: 12px;">
			<b><?php if($isi[0]->status_pembayaran==1){ echo 'LUNAS'; }else{ echo $isi[0]->keterangan_pembayaran; } ?></b><br><br>
			</div>
			<div class="col-xs-12" style=" text-align: center; font-size: 10px;">
			Terima Kasih.<br><br>
			</div>
			<div class="col-xs-12" style=" text-align: center; font-size: 10px;">
			Barang yang telah diterima tidak dapat dikembalikan dan Barang yang sudah jadi tidak di ambil lebih dari 2 minggu diluar tanggung jawab kami.
			</div>
        </div>
    </div>
        	
</section>