<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Total Pembelanjaan</h3>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
				<form class="form-inline" action="<?php echo base_url('kasir/pembayaran/update_keterangan'); ?>"  method="post">
					<div class="row" style="margin-left:10px; margin-bottom:10px">
						<div class="col-md-5">
							<div class="input-group">
                                        <input type="text" name="keterangan" value="<?php echo $isi[0]->keterangan_pembayaran; ?>" placeholder="Keterangan" class="form-control">
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-flat"  type="submit">Update Keterangan!</button>
                                        </span>
										<input type="hidden" name="voucher" value="<?php echo $isi[0]->voucher; ?>" />
                                    </div>
						</div>
					</div>
				</form>
                <form action="<?php echo base_url('kasir/pembayaran/detail'); ?>"  method="post">
                <table class="table table-striped">
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Keterangan</th>
                        <th>Harga</th>
                        <th>QTY</th>
                        <th>Finishing</th>
						<th>QTY</th>
                        <th>Jumlah</th>
                    </tr>
                    <?php $n=1; $total=0; $keterangan=""; foreach ($isi as $row){ ?>
                    <tr>
                        <td><?php echo $n; ?>.</td>
                        <td><?php echo $row->jenis_kasir.'-'.$row->ukuran.'-'.$row->bahan; ?></td>
                        <td><?php echo $row->harga_satuan; ?></td>
                        <td><?php echo $row->qty; ?></td>
                        <td><?php echo $row->nama_finishing.'('.$row->harga_finishing.')'; ?></td>
						<td><?php echo $row->finishing_qty; ?></td>
                        <td><?php echo $row->harga_total; ?></td>
                    </tr>
                    <?php $n++; $total = $row->harga_total + $total; $keterangan =  $keterangan.', '.$row->jenis_kasir.'-'.$row->ukuran.'-'.$row->bahan; } ?>
                    <tr>
                        <th colspan="6"><h3>Total</h3></th>
                        <th><h3>Rp. <?php echo number_format($total)?></h3></th>
                    </tr>
					<tr>
                        <th colspan="5"><h3>Keterangan</h3></th>
                        <th><h3><?php echo $row->keterangan_pembayaran; ?></h3></th>
                    </tr>
					<tr>
                        <th colspan="6"><h3>Status</h3></th>
                        <th><h3><?php if($row->status_pembayaran==1){ echo '<a href="'.base_url("kasir/pembayaran/update_status").'/'.$isi[0]->voucher.'/0"><input type="button" class="btn btn-success" value="LUNAS"/></a>'; }else{ echo '<a href="'.base_url("kasir/pembayaran/update_status").'/'.$isi[0]->voucher.'/1"><input type="button" class="btn btn-danger" value="Belum Lunas"</a>'; } ?></h3></th>
                    </tr>
                    <input type="hidden" name="total" value="<?php echo $total; ?>" />
                    <input type="hidden" name="keterangan" value="<?php echo $keterangan; ?>" />
                </table>
                <br>
                <div style="padding:10px;">
                    <input type="button" class="btn btn-success" value="Print"  onClick="window.open('<?php echo base_url().'kasir/pembayaran/print_pembayaran/'.$voucher; ?>','name','height=500,width=500')">
                </div>
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>