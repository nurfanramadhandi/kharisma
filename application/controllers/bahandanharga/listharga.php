<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Listharga extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('session_logged') != TRUE) {
		 	$this->load->view('login');
		}

		 $this->load->model('global_model');
	}

	public function index()
	{
		$data['tampil']=$this->db->query("SELECT * FROM view_harga_satuan ")->result();
		$data['bahan']=$this->db->query("SELECT * FROM tbl_bahan")->result();
		$data['ukuran']=$this->db->query("SELECT * FROM tbl_ukuran")->result();

		$data['page'] = 'bahandanharga/listharga';
		$this->load->view('template',$data);
		
	}

	function save_data()
	{
		$data['kode_ukuran'] = $this->input->post('ukuran');
		$data['kode_bahan'] = $this->input->post('bahan'); 
		$data['harga_satuan']=$this->input->post('harga');
		$data['kode_jenis_kasir']='pr';

		

		$this->global_model->save_data($data,'tbl_harga_satuan');

		redirect('bahandanharga/listharga');
	}

	function get_data($id)
	{

		$data['tampil']=$this->global_model->get_data('*',$id,'id','tbl_harga_satuan')->result();
		

		$data['page'] = 'bahandanharga/editharga';
		$this->load->view('template',$data);
	}

	function update_data()
	{
		$id=$this->input->post('id');

		$data['id']=$this->input->post('id');
		$data['kode_bahan']=$this->input->post('bahan');
		$data['kode_ukuran']=$this->input->post('ukuran');
		$data['harga_satuan']=$this->input->post('harga_satuan');
		$data['kode_jenis_kasir']='pr';

		$this->db->where('id',$id);
		$this->db->update('tbl_harga_satuan',$data);

		redirect('bahandanharga/listharga');
	}

	function delete_data($id)
	{

       $this->global_model->delete_data($id,'id','tbl_harga_satuan');
        
        redirect('bahandanharga/listharga/');

	}

}

/* End of file listharga.php */
/* Location: ./application/controllers/listharga.php */