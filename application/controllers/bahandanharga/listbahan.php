<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Listbahan extends CI_Controller {
 
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('session_logged') != TRUE) {
		 	$this->load->view('login');
		}

		 $this->load->model('global_model');
	}

	public function index()
	{
		$data['tampil']=$this->db->query("SELECT * FROM tbl_bahan ")->result();

		$data['page'] = 'bahandanharga/listbahan';
		$this->load->view('template',$data);
	}

	function save_data()
	{
		$data['kode_bahan'] = $this->input->post('kd_bahan');
		$data['bahan'] = $this->input->post('nama_bahan');

		$this->global_model->save_data($data,'tbl_bahan');

		redirect('bahandanharga/listbahan');
	}

	function get_data($id)
	{

		$data['tampil']=$this->global_model->get_data('*',$id,'id','tbl_bahan')->result();

		$data['page'] = 'bahandanharga/editbahan';
		$this->load->view('template',$data);
	}

	function update_data()
	{
		$id=$this->input->post('id_bahan');

		$data['id']=$this->input->post('id_bahan');
		$data['kode_bahan']=$this->input->post('kode_bahan');
		$data['bahan']=$this->input->post('nama_bahan');

		$this->global_model->update_data($id,'id',$data,'tbl_bahan');

		redirect('bahandanharga/listbahan');
	}

	function delete_data($id)
	{

       $this->global_model->delete_data($id,'id','tbl_bahan');
        
        redirect('bahandanharga/listbahan/');

	}

}

/* End of file listbahan.php */
/* Location: ./application/controllers/listbahan.php */