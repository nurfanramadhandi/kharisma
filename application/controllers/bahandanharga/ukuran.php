<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ukuran extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('session_logged') != TRUE) {
		 	$this->load->view('login');
		}

		 $this->load->model('global_model');
	}

	public function index()
	{
		$data['tampil']=$this->db->query("SELECT * FROM tbl_ukuran ")->result();

		$data['page'] = 'bahandanharga/ukuran';
		$this->load->view('template',$data);
	}


	function save_data()
	{
		$data['kode_ukuran'] = $this->input->post('kode_ukuran');
		$data['ukuran'] = $this->input->post('nama_ukuran');

		$this->global_model->save_data($data,'tbl_ukuran');

		redirect('bahandanharga/ukuran');
	}

	function get_data($id)
	{

		$data['tampil']=$this->global_model->get_data('*',$id,'id','tbl_ukuran')->result();

		$data['page'] = 'bahandanharga/editukuran';
		$this->load->view('template',$data);
	}

	function update_data()
	{
		$id=$this->input->post('id_ukuran');

		$data['id']=$this->input->post('id_ukuran');
		$data['kode_ukuran']=$this->input->post('kode_ukuran');
		$data['ukuran']=$this->input->post('ukuran');

		$this->global_model->update_data($id,'id',$data,'tbl_ukuran');

		redirect('bahandanharga/ukuran');
	}

	function delete_data($id)
	{

       $this->global_model->delete_data($id,'id','tbl_ukuran');
        
        redirect('bahandanharga/ukuran/');

	}

}

/* End of file ukuran.php */
/* Location: ./application/controllers/ukuran.php */